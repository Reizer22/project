//
//  Note_Parsing.swift
//  Notes
//
//  Created by admin on 20.04.17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import UIKit
import CocoaLumberjack

extension Note {
    var json : [String:Any] {
        var resultJson = [String:Any]()
        
        resultJson["uid"] = uid
        resultJson["title"] = title
        resultJson["content"] = content
        
        if color != UIColor.white {
            resultJson["color"] = color.toHexString
        }
        if let deleteDate = deleteDate {
            resultJson["destroy_date"] = Int(deleteDate.timeIntervalSince1970)
        }
        return resultJson
    }
    
    static func parse(json: [String:Any])->Note? {
        var colorNote = UIColor.clear
        var dateNote:Date? = nil
        
        if let jsonColor = json["color"] as? String {
            if let color = UIColor(hex: jsonColor) {
                colorNote = color
            }
            else {
                DDLogError("Error. Invalid color format.")
                return nil
            }
        }
        else {
            colorNote = UIColor.white
        }
        if let jsonDate = json["destroy_date"] as? Int {
            dateNote = Date(timeIntervalSince1970: TimeInterval(jsonDate))
        }
        guard let uid = json["uid"] as? String,
            let title = json["title"] as? String,
            let content = json["content"] as? String
            else {
                DDLogError("Error. Invalid format.")
                return nil
        }
        return Note(uid: uid, title: title, content: content, color: colorNote, deleteDate: dateNote)
    }
}

