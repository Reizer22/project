//
//  CustomCollectionViewCell.swift
//  Notes
//
//  Created by admin on 13.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var content: UILabel!
    
    @IBOutlet weak var colorView: TriangleView!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var editView: UIView!
    
}
