//
//  ColorPickerView.swift
//  Notes
//
//  Created by admin on 04.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class ColorPickerView: UIView {
    
    var colorTarget: UIColor = UIColor.black
    
    override func draw(_ rect: CGRect) {
        let size: CGFloat = rect.maxX
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        //ellipse
        context.setStrokeColor(UIColor.black.cgColor)
        context.setLineWidth(2)
        let ellipseX: CGFloat = size*0.1
        let ellipseY: CGFloat  = size*0.1
        let ellipseDiagonal = size*0.8
        
        let circleRect = CGRect.init(x: ellipseX, y: ellipseY, width: ellipseDiagonal, height: ellipseDiagonal)
        
        context.setFillColor(colorTarget.cgColor)
        context.fillEllipse(in: circleRect)
        context.strokeEllipse(in: circleRect)
        //target
        context.move(to: CGPoint(x: 0, y: size/2))
        context.addLine(to: CGPoint(x: size*0.1, y: size/2))
        context.move(to: CGPoint(x: size*0.9, y: size/2))
        context.addLine(to: CGPoint(x: size, y: size/2))
        context.move(to: CGPoint(x: size/2, y: 0))
        context.addLine(to: CGPoint(x: size/2, y: size*0.1))
        context.move(to: CGPoint(x: size/2, y: size*0.9))
        context.addLine(to: CGPoint(x: size/2, y: size))
        context.strokePath()
    }

}
