//
//  TriangleView.swift
//  Notes
//
//  Created by admin on 14.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class TriangleView: UIView {
    
    var backColor = UIColor.white

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        let path = UIBezierPath.init()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: rect.maxX, y: 0))
        path.addLine(to: CGPoint(x: 0, y: rect.maxY))
        path.close()
        backColor.set()
        path.fill()
    }

}
