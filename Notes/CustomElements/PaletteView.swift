//
//  PaletteView.swift
//  Notes
//
//  Created by admin on 04.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class PaletteView: UIView {
    
    var brightness: Float? = nil
    var selectedColor: UIColor? = nil

    override func draw(_ rect: CGRect)
    {
        if let color = selectedColor {
            let colorView = ColorView.init(frame: rect)
            colorView.squareColor = color
            colorView.selected = true
            colorView.draw(rect)
        }
        else {
        
            guard let context = UIGraphicsGetCurrentContext() else {
                return
            }
            
            let locations: [CGFloat] = [ 0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1]
            
            var colors = [UIColor.red.cgColor,
                          UIColor.orange.cgColor,
                          UIColor.yellow.cgColor,
                          UIColor.green.cgColor,
                          UIColor.cyan.cgColor,
                          UIColor.blue.cgColor,
                          UIColor.magenta.cgColor,
                          UIColor.purple.cgColor]
            
            let colorspace = CGColorSpaceCreateDeviceRGB()
            
            let colorsGradient = CGGradient(colorsSpace: colorspace, colors: colors as CFArray, locations: locations)
            
            var startPoint = CGPoint()
            var endPoint =  CGPoint()
            
            startPoint.x = 0.0
            startPoint.y = rect.maxY
            endPoint.x = rect.maxX
            endPoint.y = rect.maxY
            
            context.drawLinearGradient(colorsGradient!,
                                        start: startPoint, end: endPoint, 
                                        options: .drawsBeforeStartLocation)
            
            colors = [UIColor.clear.cgColor, UIColor.white.cgColor]
            let whiteGradient = CGGradient(colorsSpace: colorspace, colors:colors as CFArray, locations: [0, 1])
            context.drawLinearGradient(whiteGradient!, start: CGPoint.init(x: 0, y: 0), end: CGPoint.init(x: 0, y: rect.maxY), options: .drawsBeforeStartLocation)
            
            context.setLineWidth(4.0)
            context.setStrokeColor(UIColor.black.cgColor)
            context.move(to: CGPoint.zero)
            context.addLine(to: CGPoint(x: rect.maxX, y: 0))
            context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
            context.addLine(to: CGPoint(x: 0, y: rect.maxY))
            context.addLine(to: CGPoint.zero)
            context.strokePath()
            
            if let bright = brightness {
                context.setFillColor(gray: 0, alpha: CGFloat(bright))
                context.fill(rect)
            }
        }
        
    }

}
