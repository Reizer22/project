//
//  ColorView.swift
//  Notes
//
//  Created by admin on 03.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class ColorView: UIView {
    
    @IBInspectable var squareColor: UIColor?
    var selected: Bool = false

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        if let color = squareColor {
            context.setFillColor(color.cgColor)
        }
        else {
            context.setFillColor(UIColor.white.cgColor)
        }
        context.setLineWidth(4)
        context.setStrokeColor(UIColor.black.cgColor)
        let rectangle = CGRect(x: 2,y: 2,width: rect.maxX-4,height: rect.maxY-4)
        context.addRect(rectangle)
        context.strokePath()
        context.fill(rectangle)
        
        if selected {
            //ellipse
            context.setLineWidth(2)
            let ellipseX = rect.maxX-rect.maxX/3
            let ellipseY = rect.maxY/3-rect.maxY/4
            let ellipseDiagonal = rect.maxX/4
            let ellipseRad = ellipseDiagonal/2
            
            let circleRect = CGRect.init(x: ellipseX, y: ellipseY, width: ellipseDiagonal, height: ellipseDiagonal)
            context.strokeEllipse(in: circleRect)
            //check
            context.move(to: CGPoint(x: ellipseX+4, y: ellipseY+ellipseRad))
            context.addLine(to: CGPoint(x: ellipseX+ellipseRad, y: ellipseY+ellipseDiagonal-4))
            context.addLine(to: CGPoint(x: ellipseX+ellipseDiagonal-4, y: ellipseY+4))
            context.strokePath()
        }
        
    }
}
