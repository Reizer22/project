//
//  Note_Struct.swift
//  Notes
//
//  Created by admin on 20.04.17.
//  Copyright © 2017 admin. All rights reserved.
//
import UIKit
import Foundation

struct Note {
    let uid: String
    let title: String
    let content:String
    let color:UIColor
    let deleteDate:Date?
    
    init(uid: String = UUID().uuidString, title: String, content:String, color:UIColor = UIColor.white, deleteDate:Date? = nil) {
        
        self.uid = uid
        self.color = color
        self.title = title
        self.content = content
        self.deleteDate = deleteDate
    }
}
