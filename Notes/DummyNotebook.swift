//
//  DummyNotebook.swift
//  Notes
//
//  Created by admin on 18.04.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CocoaLumberjack

class DummyNotebook:NSObject {
    private(set) var notes = [Note]()
    
    var filePath:String? {
        guard let dir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .allDomainsMask, true).first else {
            return nil
        }
        let path = "\(dir)/notes.plist"
        print(path)
        return path
    }
    
    func addNewNote(note: Note) {
        notes.append(note);
        if note.deleteDate != nil {
            autoDelete(note: note)
        }
    }
    
    func deleteNote(atUid uidNote:String) {
        let ids = notes.filter() {
            ($0.uid != uidNote)
        }
        
        notes = ids
    }
    
    func saveNotes() {
        if (notes.count) > 0 {
            var notesJson = [[String:Any]]()
            for n:Note in notes {
                notesJson.append(n.json)
            }
            guard let data = try? JSONSerialization.data(withJSONObject: notesJson,  options:.prettyPrinted),
                let path = filePath
                else {
                    DDLogError("Error. Data invalid or path not found!")
                    return
            }
            do {
                try data.write(to: URL.init(fileURLWithPath:path))
            }
            catch {
                DDLogError("Error data write!")
            }
        }
        else {
            DDLogInfo("Notes is empty.")
        }
        
    }
    
    func uploadNotes() {
        guard let path = filePath,
            let data = try? Data(contentsOf: URL(fileURLWithPath:path)),
            let jsonData = try? JSONSerialization.jsonObject(with: data),
            let jsonNotes = jsonData as? [[String:Any]]
            else {
                DDLogError("Error upload json!")
                return
        }
        for jsonNote in jsonNotes {
            if let Note = Note.parse(json: jsonNote) {
                notes.append(Note)
                if Note.deleteDate != nil {
                    autoDelete(note: Note)
                }
            }
            else {
                DDLogError("Json not upload. Error parse json to note!")
                return
            }
        }
    }
    
    func autoDelete(note:Note) {
        if note.deleteDate! < Date() {
            self.deleteNote(atUid: note.uid)
        }
        else {
            let timeIntervalInSeconds = note.deleteDate!.timeIntervalSince(Date())
            delayWithSeconds(timeIntervalInSeconds) {
                self.deleteNote(atUid: note.uid)
            }
        }
    }
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now()+seconds) {
            completion()
        }
    }
    
}

