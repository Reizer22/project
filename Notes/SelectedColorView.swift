//
//  SelectedColorView.swift
//  Notes
//
//  Created by admin on 04.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class SelectedColorView: UIView {
    
    var color = UIColor.white
    

    override func draw(_ rect: CGRect)
    {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        context.setFillColor(color.cgColor)
        context.setLineWidth(4)
        context.setStrokeColor(UIColor.black.cgColor)
        let rectangle = CGRect(x: 0,y: 0,width: rect.maxX,height: rect.maxX)
        context.addRect(rectangle)
        context.strokePath()
        context.fill(rectangle)
    }
    
    override func layoutSubviews() {
        self.round(corners: UIRectCorner.allCorners, radius: 5, borderColor: UIColor.black, borderWidth: 3)
    }
}
