//
//  ColorPickerVC.swift
//  Notes
//
//  Created by admin on 04.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

protocol ApplyChangesDelegate {
    func saveAndSelect(paletteColor :UIColor?, andPoint target:CGPoint, andBrightness value:Float)
}

class ColorPickerVC: UIViewController {

    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var colorPicker: ColorPickerView!
    @IBOutlet weak var paletteView: PaletteView!
    @IBOutlet weak var selectedHexLabel: UILabel!
    @IBOutlet weak var changedBrightness: UISlider!
    @IBOutlet weak var selectedColorView: SelectedColorView!
    
    var selectedColor: UIColor?
    var delegate: ApplyChangesDelegate?
    var colorPickerPoint: CGPoint = CGPoint.init(x: 0, y: 0)
    var brightness: Float = 0
    
    override func viewWillTransition(to size: CGSize, with coordinator:
        UIViewControllerTransitionCoordinator) {
        paletteView.setNeedsDisplay()
    }
    
    override func viewDidLayoutSubviews() {
     //   paletteView.frame.size = self.paletteView.bounds.size
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        changedBrightness.setValue(brightness, animated: true)
        if let color = selectedColor {
            paletteView.brightness = changedBrightness.value
            paletteView.setNeedsDisplay()
            selectedHexLabel.text = color.toHexString
            selectedColorView.color = color
            selectedColorView.setNeedsDisplay()
            colorPicker.isHidden = false
            colorPicker.center = colorPickerPoint
            colorPicker.colorTarget = color
            colorPicker.setNeedsDisplay()
        }
        else {
            colorPicker.center = CGPoint.init(x: paletteView.frame.width/2, y: paletteView.frame.height/2)
            setSelectedColor(x: colorPicker.center.x, y: colorPicker.center.y)
        }
    }
   
    @IBAction func changedBrightness(_ sender: Any, forEvent event: UIEvent) {
        paletteView.brightness = changedBrightness.value
        paletteView.setNeedsDisplay()
        if selectedColor != nil {
            setSelectedColor(x: colorPicker.center.x, y: colorPicker.center.y)
            colorPicker.setNeedsDisplay()
        }
    }
    
    @IBAction func panToColor(_ sender: UIPanGestureRecognizer) {
 
        if (sender.location(in: paletteView).x > 1 && sender.location(in: paletteView).x < paletteView.frame.width-2 && sender.location(in: paletteView).y > 1 && sender.location(in: paletteView).y < paletteView.frame.height-2) {
            
            colorPicker.center = CGPoint.init(x: sender.location(in: paletteView).x, y: sender.location(in: paletteView).y)
            colorPicker.translatesAutoresizingMaskIntoConstraints = false
            setSelectedColor(x: sender.location(in: paletteView).x, y: sender.location(in: paletteView).y)
        }
        
    }
   
    
    func setSelectedColor(x: CGFloat, y: CGFloat) {
        let color = paletteView.colorOfPoint(point: CGPoint.init(x: x, y: y))
        selectedColorView.color = color
        colorPicker.setNeedsDisplay()
        selectedColor = color
        selectedHexLabel.text = "#"+color.toHexString
        colorPicker.colorTarget = color
        selectedColorView.setNeedsDisplay()
    }
    
    @IBAction func doneActionButton(_ sender: Any) {
        delegate?.saveAndSelect(paletteColor: selectedColor, andPoint: colorPicker.center, andBrightness: changedBrightness.value)
    }

    
}
