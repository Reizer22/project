//
//  AutorizationVC.swift
//  Notes
//
//  Created by admin on 23.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class AutorizationVC: UIViewController,UIWebViewDelegate {

    let authID = "391eee4ba5534d36915717f509732dbc"
    let callbackURL = "https://oauth.yandex.ru/verification_code"
    private var token: String?
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: "https://oauth.yandex.ru/authorize?response_type=token&client_id=\(authID)&display=popup")!
        let urlRequest = URLRequest(url: url)
        webView.loadRequest(urlRequest)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        let url = request.url!
        
        if url.description.contains(callbackURL) {
            let result = url.description.components(separatedBy: CharacterSet(charactersIn: "#=&"))
            token = result[2]
            
            performSegue(withIdentifier: "gotoListNotes", sender: nil)
            
            return false
        }
        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoListNotes" {
            let navController = segue.destination as! UINavigationController
            let listNotesVC = navController.topViewController as! ListNotesVC
            listNotesVC.token = token
        }
    }

}
