//
//  ListNotesVC.swift
//  Notes
//
//  Created by admin on 13.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class ListNotesVC: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource,notesDelegate {
    @IBOutlet weak var zoomSlider: UISlider!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var editButton: UIBarButtonItem!

    var operations = Operations()
    var editMode = false
    var notes = [Note]()
    var token: String?
    
    func reloadNotes() {
        collectionView.reloadData()
    }
    
    override func viewWillLayoutSubviews() {
        setSizeCell()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        collectionView.reloadSections(.init(integer: 0))
        setSizeCell()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = editButtonItem;
        editButtonItem.title = NSLocalizedString("Edit", tableName: "Изменить", comment: "")
        collectionView.contentInset = .init(top: 5, left: 5, bottom: 5, right: 5)
        
        operations = Operations(controller: self)
        operations.delegate = self
        operations.getList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoteCell", for: indexPath) as! CustomCollectionViewCell
        
        cell.title.text = notes[indexPath.row].title
        cell.content.text = notes[indexPath.row].content
        cell.colorView.backColor = notes[indexPath.row].color
        cell.colorView.setNeedsDisplay()
        cell.deleteButton.tag = indexPath.row
        
        if editMode {
            cell.editView.isHidden = false
            cell.colorView.alpha = 0.2
        }
        else {
            cell.editView.isHidden = true
            cell.colorView.alpha = 1
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if editMode == false {
            print(indexPath.row)
            performSegue(withIdentifier: "gotoEditNote", sender: indexPath.row)
        }
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        if editMode {
            editButtonItem.title = NSLocalizedString("Edit", tableName: "Изменить", comment: "")
            editMode = false
        }
        else {
            editButtonItem.title = NSLocalizedString("Done", tableName: "Готово", comment: "")
            editMode = true
        }
        collectionView.reloadSections(.init(integer: 0))
    }
    
    
    @IBAction func zoomSlider(_ sender: Any) {
        setSizeCell()
    }
    
    func setSizeCell() {
        let minWidth = Float(collectionView.frame.width)/3.3
        let layout = UICollectionViewFlowLayout()
        let currentWidth:Float = minWidth * zoomSlider.value
        layout.itemSize = CGSize.init(width: Int(currentWidth), height: 100)
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    @IBAction func deleteButton(_ sender: Any, forEvent event: UIEvent) {
        let index = IndexPath.init(item: (sender as AnyObject).tag, section: 0)

        operations.deleteNote(atUid: self.notes[(sender as AnyObject).tag].uid, andIndex: index)
    }
    
    func deleteAndReloadCollection(atIndex: IndexPath) {

        collectionView.performBatchUpdates({
            self.collectionView.deleteItems(at: [atIndex])
            
        }, completion: { (finished:Bool) -> Void in
            if finished {
                self.collectionView.reloadData()
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoEditNote" {
            let controller = segue.destination as! EditNoteVC
            controller.operations = operations
            if let row = sender as? Int {
                controller.note = notes[row]
            }
        }
    }

}
