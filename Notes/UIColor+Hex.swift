//
//  UiColor_Hex.swift
//  Notes
//
//  Created by admin on 20.04.17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    var toHexString: String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return String(format: "#%02X%02X%02X", Int(r*0xff), Int(g*0xff), Int(b*0xff))
    }
    
    convenience init?(hex: String) {
        let index = hex.index(hex.startIndex, offsetBy: 1)
        let numbers = hex.substring(from: index)
        let scan = Scanner(string: numbers)
        scan.scanLocation = 0
        var rgb:UInt64 = 0
        if scan.scanHexInt64(&rgb) {
            let r = (rgb & 0xff0000) >> 16
            let g = (rgb & 0xff00) >> 8
            let b = rgb & 0xff
            
            self.init(red: CGFloat(r)/0xff, green:CGFloat(g)/0xff, blue: CGFloat(b)/0xff, alpha:1)
            return
        }
        
        return nil
    }
}
