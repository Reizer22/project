//
//  ViewController.swift
//  Notes
//
//  Created by admin on 18.04.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class EditNoteVC: UIViewController, ApplyChangesDelegate, UITextFieldDelegate, UITextViewDelegate,UIActionSheetDelegate {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var switcher: UISwitch!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var thirdColorView: ColorView!
    @IBOutlet weak var secondColorView: ColorView!
    @IBOutlet weak var firstColorView: ColorView!
    @IBOutlet weak var paletteColorView: PaletteView!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var colorsViewTopConstraint: NSLayoutConstraint!
    
    var colorPickerPoint: CGPoint?
    var brightness: Float = 0
    private var child: UIViewController!
    private var effect: UIVisualEffectView!
    
    var operations: Operations?
    var note: Note?
    var selectedColor = UIColor.white
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if let n = note {
            navigationItem.title = "Редактирование"
            titleTextField.text = n.title
            contentTextView.text = n.content
            paletteColorView.selectedColor = n.color
            selectedColor = n.color
            paletteColorView.setNeedsDisplay()
            if let deleteDate = n.deleteDate {
                switcher.isOn = true
                datePicker.date = deleteDate
            }
            else {
                switcher.isOn = false
                tapToSwitch((Any).self)
            }
        }
        else {
            navigationItem.title = "Новая заметка"
        }
        
    }

    @IBAction func tappedToFirstColorView(_ sender: Any) {
        selectColorView(firstColorView)
    }
    
    @IBAction func tappedToSecondColorView(_ sender: Any) {
        selectColorView(secondColorView)
    }
    
    @IBAction func tappedToThirdColorView(_ sender: Any) {
        selectColorView(thirdColorView)
    }
    
    @IBAction func longTapToPalette(_ sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizerState.ended {
      
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = contentView.bounds
            effect = blurEffectView
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin, .flexibleRightMargin, . flexibleTopMargin, . flexibleBottomMargin]
            contentView.addSubview(blurEffectView)
        
            blurEffectView.alpha = 0.0
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let colorPicker = storyboard.instantiateViewController(withIdentifier: "ColorPickerVC") as! ColorPickerVC
         
            let childView = colorPicker.view
            childView?.frame = container.bounds
            child = colorPicker
            //Settings picker
            colorPicker.selectedColor = paletteColorView.selectedColor
            if let colorP = colorPickerPoint {
                colorPicker.colorPickerPoint = colorP
            }
            colorPicker.brightness = brightness
            colorPicker.delegate = self
            //Settings view
            addChildViewController(colorPicker)
            container.addSubview(colorPicker.view)
            container.isHidden = false
            childView?.transform = CGAffineTransform.init(translationX: 0, y: 100)
            childView?.alpha = 0.0
            //Animation
            UIView.animate(withDuration: 0.4, animations: {
                childView?.transform = CGAffineTransform.identity
                childView?.alpha = 1.0
                blurEffectView.alpha = 0.8
            })
            { (_) in
                            colorPicker.didMove(toParentViewController: self)
                     }

        }
    }
    
    func selectColorView(_ colorView: ColorView) {
        if colorView.selected {
            colorView.selected = false
        }
        else {
            removeSelection()
            colorView.selected = true
        }
        selectedColor = colorView.squareColor!
        colorView.setNeedsDisplay()
    }
    
    func removeSelection() {
        if firstColorView.selected {
            firstColorView.selected = false
            firstColorView.setNeedsDisplay()
        }
        if secondColorView.selected {
            secondColorView.selected = false
            secondColorView.setNeedsDisplay()
        }
        if thirdColorView.selected {
            thirdColorView.selected = false
            thirdColorView.setNeedsDisplay()
        }
        if paletteColorView.selectedColor != nil {
            colorPickerPoint = nil
            brightness = 0
            paletteColorView.selectedColor = nil
            paletteColorView.setNeedsDisplay()
        }
        
    }

    @IBAction func tapToSwitch(_ sender: Any) {
        
        if !switcher.isOn {
            datePicker.isHidden = true
            UIView.animate(withDuration: 1, animations: {
                self.colorsViewTopConstraint.constant = self.colorsViewTopConstraint.constant - self.datePicker.frame.height
                self.view.layoutIfNeeded()
            })
            
        }
        else{
            datePicker.isHidden = false
            UIView.animate(withDuration: 1, animations: {
                self.colorsViewTopConstraint.constant = self.colorsViewTopConstraint.constant + self.datePicker.frame.height
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect {

            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height + 20, 0.0);
            scrollView.contentInset = contentInsets;
            scrollView.scrollIndicatorInsets = contentInsets;
            
            var aRect = self.view.frame;
            aRect.size.height -= keyboardSize.height;
            
            if !aRect.contains(contentTextView.frame.origin) {
                scrollView.scrollRectToVisible(contentTextView.frame, animated: true)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero;
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        titleTextField.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            contentTextView.resignFirstResponder()
            return false
        }
        return true
    }
    @IBAction func tapToView(_ sender: UITapGestureRecognizer) {
        if child != nil && (sender.location(in: container).x < 0 || sender.location(in: container).y < 0 || sender.location(in: container).x > container.frame.size.width || sender.location(in: container).y > container.frame.size.height) {
            
            //print("x: \(sender.location(in: container).x) y: \(sender.location(in: container).y)")
            closeColorPickerView()
        }
        
    }
    
    func saveAndSelect(paletteColor :UIColor?, andPoint target:CGPoint, andBrightness value:Float) {
        if let color = paletteColor {
            removeSelection()
            paletteColorView.selectedColor = color
            selectedColor = color
            colorPickerPoint = target
            brightness = value
            paletteColorView.setNeedsDisplay()
        }
        //Close view
        closeColorPickerView()
        
    }
    
    func closeColorPickerView() {
        
        child.willMove(toParentViewController: nil)
        UIView.animate(withDuration: 0.4, animations: {
            self.child.view.transform = .init(translationX: 0, y: 100)
            self.child.view.alpha = 0.0
            self.effect.alpha = 0.0
        }) { (_) in
            self.child.view.removeFromSuperview()
            self.child.removeFromParentViewController()
            self.effect.removeFromSuperview()
            self.container.isHidden = true
            self.child = nil
        }
    }
    
    @IBAction func saveButton(_ sender: Any) {
        if (titleTextField.text?.isEmpty)! || contentTextView.text.isEmpty {
            UIAlertView.init(title: "", message: "Title or content is empty! Note not save!", delegate: nil, cancelButtonTitle: "Ok").show()
        }
        else {
            var deleteDate: Date? = nil
            if switcher.isOn {
                deleteDate = datePicker.date
            }
            var newNote = Note(title: titleTextField.text!, content: contentTextView.text, color: selectedColor, deleteDate: deleteDate)
            
            if let n = note {
                newNote = Note(uid: n.uid,title: titleTextField.text!, content: contentTextView.text, color: selectedColor, deleteDate: deleteDate)
                operations?.updateNote(note:newNote, atUid: n.uid)
            }
            else {
                operations?.addNote(note: newNote)
            }
            navigationController?.popViewController(animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "gotoColorPicker" {
//            let colorPicker: ColorPickerVC = segue.destination as! ColorPickerVC
//            colorPicker.selectedColor = paletteColorView.selectedColor
//            if let colorP = colorPickerPoint {
//                colorPicker.colorPickerPoint = colorP
//            }
//            colorPicker.brightness = brightness
//            colorPicker.delegate = self
//            
//        }
    }


}

