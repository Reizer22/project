//
//  Dispatcher.swift
//  Notes
//
//  Created by admin on 20.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class Dispatcher: NSObject {
    
    private let networkQueue = OperationQueue()
    private let cacheQueue = OperationQueue()
    private let taskQueue = OperationQueue()
    private let interfaceQueue = OperationQueue()
    private let defaultQueue = OperationQueue()
    
    override init() {
        networkQueue.maxConcurrentOperationCount = 5
        networkQueue.qualityOfService = .background
        cacheQueue.maxConcurrentOperationCount = 5
        cacheQueue.qualityOfService = .utility
        interfaceQueue.maxConcurrentOperationCount = 5
        interfaceQueue.qualityOfService = .userInteractive
        taskQueue.maxConcurrentOperationCount = 2
        taskQueue.qualityOfService = .userInitiated
        defaultQueue.maxConcurrentOperationCount = 2
        defaultQueue.qualityOfService = .default
        
    }
    
    func add(operation: Operation, toQueue queue: String) {
        
        switch queue {
        case "network":
            networkQueue.addOperation(operation)
        case "cache":
            cacheQueue.addOperation(operation)
        case "interface":
            interfaceQueue.addOperation(operation)
        case "task":
            taskQueue.addOperation(operation)
        default:
            defaultQueue.addOperation(operation)
        }
    }
    
    func add(code: @escaping ()->(), toQueue queue: String) {
        
        switch queue {
        case "network":
            networkQueue.addOperation(code)
        case "cache":
            cacheQueue.addOperation(code)
        case "interface":
            interfaceQueue.addOperation(code)
        case "task":
            taskQueue.addOperation(code)
        default:
            defaultQueue.addOperation(code)
        }
    }
    
}
