//
//  WorkWithNotes.swift
//  Notes
//
//  Created by admin on 20.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CocoaLumberjack

class InterfaceOperations: AsyncOperation {
    
    var listNotesController: ListNotesVC!
    var note: Note?
    var uid: String?
    
    init(onListNotes listNotesController: ListNotesVC,note: Note? = nil, uid: String? = nil) {
        self.listNotesController = listNotesController
        self.note = note
        self.uid = uid
    }
}

class AddNote: InterfaceOperations {
    
    override func main() {
        if let n = note {
            listNotesController.notes.append(n)
            DDLogInfo("Note added")
        }
        else {
            DDLogError("Note is nil!")
        }
        self.finish()
    }
}

class DeleteNote: InterfaceOperations {
    
    override func main() {
        if let id = uid {
            let ids = listNotesController.notes.filter() {
                ($0.uid != id)
            }
            listNotesController.notes = ids
            DDLogInfo("Note deleted")
        }
        else {
            DDLogError("UID is nil!")
        }
        self.finish()
    }
}

