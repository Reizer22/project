//
//  Network.swift
//  Notes
//
//  Created by admin on 20.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CocoaLumberjack

class NetworkOperation : AsyncOperation {
    
    var listNotesController: ListNotesVC!
    var dispatcher: Dispatcher!
    var note: Note?
    
    init(note:Note? = nil, withListNotes controller: ListNotesVC, andDispatcher dispatcher: Dispatcher) {
        self.listNotesController = controller
        self.dispatcher = dispatcher
        self.note = note
    }
    
    func urlRequest(url: String) -> URLRequest? {
        guard let url = URL(string: url)
            else {
                DDLogError("Uncorrect URL string!")
                return nil
        }
        var request = URLRequest(url: url)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue("OAuth \(self.listNotesController.token!)", forHTTPHeaderField: "Authorization")
        
        return request
    }
    
    func retry(operation: NetworkOperation, whenResult json: [String:Any]) {
        if json.keys.contains("error") {
            if(json["message"] as! String).contains("Simulated error") {
                self.dispatcher.add(operation: operation, toQueue: "network")
            }
            else {
                DDLogError("Error request.")
                operation.finish()
            }
        }
    }
    
    
}

class RequestServer: NetworkOperation {
    
    override func main() {
        
        var request = urlRequest(url: "http://notes.mrdekk.ru/notes")
        request?.httpMethod = "GET"
        guard let req = request
        else {
            DDLogError("Request is nil")
            return
        }
        let task = URLSession.shared.dataTask(with: req) {
            (data, response, error) in
       
            guard let data = data, let _:URLResponse = response, error == nil,
                let jsonData = try? JSONSerialization.jsonObject(with: data)
                else {
                    DDLogError("Error dataTask")
                    return
            }
            if let jsonNotes = jsonData as? [[String:Any]] {
                for jNote in jsonNotes {
                    self.listNotesController.notes.append(Note.parse(json: jNote)!)
                }
                DDLogInfo("networkRequestComplete")
                DispatchQueue.main.async {
                    self.listNotesController.reloadNotes()
                }
                self.finish()
            }
            else if let json = jsonData as? [String:Any] {
                let retryRequest = RequestServer(withListNotes: self.listNotesController, andDispatcher: self.dispatcher)
                self.retry(operation: retryRequest, whenResult: json)
                retryRequest.success = {
                    self.finish()
                }
            }
        }
        task.resume()
    }
}

//other network services

class PostQuery: NetworkOperation {
    
       override func main() {
        
        guard let data = try? JSONSerialization.data(withJSONObject: self.note?.json as Any, options: .prettyPrinted)
            else {
                DDLogError("Uncorrect URL string or convert json to data failed!")
                return
        }
        var request = urlRequest(url:"http://notes.mrdekk.ru/notes")
        request?.httpMethod = "POST"
        request?.httpBody = data
        guard let req = request
            else {
                DDLogError("Request is nil")
                return
        }
        let task = URLSession.shared.dataTask(with: req, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    self.retry(operation: PostQuery.init(note: self.note, withListNotes: self.listNotesController, andDispatcher: self.dispatcher), whenResult: json)
                    print(json)
                    self.finish()
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
            print("networkPostQuery")
        })
        task.resume()
        self.finish()
    }
}

class PutQuery: NetworkOperation {
    
    override func main() {
        
        guard let data = try? JSONSerialization.data(withJSONObject: note?.json as Any, options: .prettyPrinted)
            else {
                DDLogError("Convert json to data failed!")
                return
        }
        var request = urlRequest(url: "http://notes.mrdekk.ru/notes/\(note!.uid.lowercased())")
        request?.httpMethod = "PUT"
        request?.httpBody = data
        guard let req = request
            else {
                DDLogError("Request is nil")
                return
        }
        let task = URLSession.shared.dataTask(with: req, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    self.retry(operation: PutQuery.init(note: self.note, withListNotes: self.listNotesController, andDispatcher: self.dispatcher), whenResult: json)
                    print(json)
                    self.finish()
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
            print("networkPutQuery")
        })
        task.resume()
        self.finish()
    }
}

class DeleteQuery: NetworkOperation {
    var uid: String!
    
    override func main() {
        
        var request = urlRequest(url: "http://notes.mrdekk.ru/notes/\(uid!.lowercased())")
        request?.httpMethod = "DELETE"
        
        guard let req = request
            else {
                DDLogError("Request is nil")
                return
        }
        
        let task = URLSession.shared.dataTask(with: req, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    let deleteQuery = DeleteQuery.init(withListNotes: self.listNotesController, andDispatcher: self.dispatcher)
                    deleteQuery.uid = self.uid
                    self.retry(operation: deleteQuery, whenResult: json)
                    print(json)
                    self.finish()
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
            print("networkDeleteQuery")
        })
        task.resume()
        
        self.finish()
    }
}
