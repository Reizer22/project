//
//  AsyncOperation.swift
//  Notes
//
//  Created by admin on 20.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class AsyncOperation: Operation {

    var success: (() -> ())? = nil
    
    fileprivate var _executing = false
    fileprivate var _finished = false
    
    override func start() {
        guard !isCancelled else {
            print("Operation is cancelled!")
            return
        }
        willChangeValue(forKey: "isExecuting")
        _executing = true
        main()
        didChangeValue(forKey: "isExecuting")
    }
    
    override func main() {
        finish()
    }
    
    func finish() {
        if isCancelled == true {
            return
        }
        success?()
        
        willChangeValue(forKey: "isFinished")
        _finished = true
        didChangeValue(forKey: "isFinished")
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    override var isExecuting: Bool {
        return _executing
    }
    
    override var isFinished: Bool {
        return _finished
    }
}
