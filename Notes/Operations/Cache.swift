//
//  Cache.swift
//  Notes
//
//  Created by admin on 20.05.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import CoreData
import CocoaLumberjack

class CacheOperation: AsyncOperation {
    
    var listNotesController: ListNotesVC?
    var coordinator: NSPersistentStoreCoordinator?
    var objectContext: NSManagedObjectContext?
    
    
    init(listNotesController: ListNotesVC) {
        self.listNotesController = listNotesController
        guard let url = Bundle.main.url(forResource: "Model", withExtension: "momd") else {
            DDLogError("URL not found!")
            return
        }
        guard let model = NSManagedObjectModel(contentsOf: url) else {
            DDLogError("model is nil!")
            return
        }
        coordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
        
        objectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        objectContext?.persistentStoreCoordinator = coordinator
        
        do {
            guard let pCoordinator = coordinator,
                let docurl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
                else {
                    return
            }
            let storeurl = docurl.appendingPathComponent("model.sqlite")
            _ = try pCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeurl, options: [NSMigratePersistentStoresAutomaticallyOption: NSNumber(value: true as Bool), NSInferMappingModelAutomaticallyOption: NSNumber(value: true as Bool)])
        } catch {
            DDLogError("error request")
        }
        
    }
}

class LoadCache: CacheOperation {

    override func main() {
        
        self.objectContext?.performAndWait {
            let request: NSFetchRequest<NoteEntity> = NoteEntity.fetchRequest()
            do {
                guard let result = try self.objectContext?.fetch(request) else {
                    DDLogError("error result")
                    return
                }
                for note in result {
                    guard let uid = note.uid, let title = note.title, let content = note.content, let hex = note.color, let color = UIColor(hex: hex) else {
                        DDLogError("Note is not valid")
                        return
                    }
                    let n = Note(uid: uid, title: title, content: content, color: color, deleteDate: note.deleteDate as Date?)
                    self.listNotesController?.notes.append(n)
                }
                DDLogInfo("loadCacheComplete")
            } catch {
                DDLogError("error result")
            }
        }
        self.finish()
    }
}

class SaveNoteInCache: CacheOperation {
    
    var note: Note?
    
    override func main() {
        
        self.objectContext?.performAndWait {
            guard let context = self.objectContext,
                let description = NSEntityDescription.entity(forEntityName: "Note", in: context)
                else {
                    DDLogError("Error in description!")
                    return
            }
            let noteCache = NoteEntity(entity: description, insertInto: self.objectContext)
            noteCache.uid = self.note?.uid.lowercased()
            noteCache.title = self.note?.title
            noteCache.content = self.note?.content
            noteCache.color = self.note?.color.toHexString
            if let del = self.note?.deleteDate {
                noteCache.deleteDate = del as NSDate
            }
            do {
                try self.objectContext?.save()
                DDLogInfo("Save note in cache complete.")
            } catch {
                DDLogError("Note not save in core data!")
            }
        }
        self.finish()
    }
}


class DeleteNoteFromCache: CacheOperation {
    
    var uid: String?
    
    override func main() {
        
        self.objectContext?.performAndWait {
            guard let id = self.uid else {
                DDLogError("uid is nil")
                return
            }
            let pred = NSPredicate(format: "uid == %@", id.lowercased())
            let request: NSFetchRequest<NoteEntity> = NoteEntity.fetchRequest()
            request.predicate = pred
            do {
                if let noteEntity = try self.objectContext?.fetch(request) {
                    if noteEntity.count > 0 {
                        self.objectContext?.delete(noteEntity[0])
                        do {
                            try self.objectContext?.save()
                        } catch {
                            
                        }
                        DDLogInfo("deleteNoteFromCacheComplete")
                    }
                    else {
                        DDLogError("Note is not found!")
                    }
                }
            } catch {
                DDLogError("error result")
            }
        }
        self.finish()
    }
}

class UpdateNoteInCache: CacheOperation {
    
    var note: Note?
    
    override func main() {
        
        self.objectContext?.performAndWait {
            guard let n = self.note else {
                DDLogError("Note is nil!")
                return
            }
            let pred = NSPredicate(format: "uid == %@", n.uid.lowercased())
            let request: NSFetchRequest<NoteEntity> = NoteEntity.fetchRequest()
            request.predicate = pred
            
            do {
                if let noteEntity = try self.objectContext?.fetch(request) {
                    if noteEntity.count > 0 {
                        noteEntity[0].setValuesForKeys(["title":n.title,"content": n.content,"color":n.color.toHexString, "deleteDate":n.deleteDate as Any])
                        do {
                            try self.objectContext?.save()
                        } catch {
                            
                        }
                        DDLogInfo("UpdateNoteInCacheComplete")
                    }
                    else {
                        DDLogError("Note is not found!")
                    }
                }
            } catch {
                DDLogError("error result")
            }
        }
        self.finish()
    }
}

