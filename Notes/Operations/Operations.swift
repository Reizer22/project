//
//  Operations.swift
//  Notes
//
//  Created by admin on 02.06.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

@objc protocol notesDelegate {
    
    @objc optional func reloadNotes()
    @objc optional func deleteAndReloadCollection(atIndex: IndexPath)
}


class Operations: NSObject {
    
    var listNotesController: ListNotesVC!
    let dispatcher = Dispatcher()
    var delegate: notesDelegate?
    private let taskDispatch = DispatchQueue.init(label: "tasks", qos: .background)
    
    override init() {
    
    }
    
    init(controller: ListNotesVC!) {
        listNotesController = controller
    }
    
    func getList() {
        let network = RequestServer(withListNotes: listNotesController, andDispatcher: dispatcher)
        network.success = {
            if self.listNotesController.notes.count != 0 {
                for note in self.listNotesController.notes {
                    if note.deleteDate != nil {
                        self.autoDelete(note: note)
                    }
                }
            }
            else {
                let load = LoadCache(listNotesController: self.listNotesController)
                load.success = {
                    for note in self.listNotesController.notes {
                        if note.deleteDate != nil {
                            self.autoDelete(note: note)
                        }
                    }
                    DispatchQueue.main.async {
                        self.delegate?.reloadNotes!()
                    }
                }
                self.dispatcher.add(operation: load, toQueue: "cache")
            }
        }
        self.dispatcher.add(operation: network, toQueue: "network")
    }
    
    func addNote(note: Note) {
        let add = AddNote(onListNotes: listNotesController, note: note)
        add.success = {
            let post = PostQuery(note: note, withListNotes: self.listNotesController, andDispatcher: self.dispatcher)
            let save = SaveNoteInCache(listNotesController: self.listNotesController)
            save.note = note
            self.dispatcher.add(operation: post, toQueue: "network")
            self.dispatcher.add(operation: save, toQueue: "cache")
            DispatchQueue.main.async {
                self.delegate?.reloadNotes!()
            }
        }
        self.dispatcher.add(operation: add, toQueue: "interface")
        
        if note.deleteDate != nil {
            autoDelete(note: note)
        }
    }
    
    func autoDelete(note:Note) {
        dispatcher.add(code: {
            if note.deleteDate! < Date() {
                self.delegate?.reloadNotes!()
                self.deleteNote(atUid: note.uid)
            }
            else {
                let timeIntervalInSeconds = note.deleteDate!.timeIntervalSince(Date())
                self.taskDispatch.asyncAfter(deadline: .now()+timeIntervalInSeconds) {
                    print("Autodelete start")
                    self.deleteNote(atUid: note.uid)
                }
            }
        }, toQueue: "interface")
    }
    
    func deleteNote(atUid: String, andIndex:IndexPath? = nil) {
        let delete = DeleteNote(onListNotes: listNotesController, uid: atUid)
        delete.success = {
            let del = DeleteQuery(withListNotes: self.listNotesController, andDispatcher: self.dispatcher)
            del.uid = atUid
            let delCache = DeleteNoteFromCache(listNotesController: self.listNotesController)
            delCache.uid = atUid
            self.dispatcher.add(operation: del, toQueue: "network")
            self.dispatcher.add(operation: delCache, toQueue: "cache")
            DispatchQueue.main.async {
                if let index = andIndex {
                    self.delegate?.deleteAndReloadCollection!(atIndex: index)
                }
                else {
                    self.delegate?.reloadNotes!()
                }
            }
        }
        dispatcher.add(operation: delete, toQueue: "interface")
    }
    
    func updateNote(note:Note, atUid: String) {
        let delete = DeleteNote(onListNotes: listNotesController, uid: atUid)
        delete.success = {
            let add = AddNote(onListNotes: self.listNotesController, note: note)
            add.success = {
                let put = PutQuery(note: note, withListNotes: self.listNotesController, andDispatcher: self.dispatcher)
                let putCache = UpdateNoteInCache(listNotesController: self.listNotesController)
                putCache.note = note
                self.dispatcher.add(operation: put, toQueue: "network")
                self.dispatcher.add(operation: putCache, toQueue: "cache")
                self.delegate?.reloadNotes!()
            }
            self.dispatcher.add(operation: add, toQueue: "interface")
        }
        if note.deleteDate != nil {
            autoDelete(note: note)
        }
        dispatcher.add(operation: delete, toQueue: "interface")
    }
}

