//
//  DummyNotebookTests.swift
//  Notes
//
//  Created by admin on 29.04.17.
//  Copyright © 2017 admin. All rights reserved.
//

import XCTest
@testable import Notes

class DummyNotebookTests: XCTestCase {
    
    let notebook = DummyNotebook()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzzz"
        formatter.timeZone = TimeZone.autoupdatingCurrent
        
        notebook.addNewNote(note: Note(uid:"aaaa", title: "title", content: "content", color: UIColor.red))
        notebook.addNewNote(note: Note(uid:"ssss", title: "title", content: "content"))
        notebook.addNewNote(note: Note(uid:"dddd", title: "title", content: "content", color: UIColor.green, deleteDate: formatter.date(from: "2017-04-01 12:00:00+0000")))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAddNote() {
        
        XCTAssertEqual(notebook.notes.count, 3)
        XCTAssertEqual([notebook.notes[0].uid,notebook.notes[1].uid,notebook.notes[2].uid], ["aaaa","ssss","dddd"])
    }
    
    func testDeleteNoteAtUid() {
        notebook.deleteNote(atUid: "ssss")
        XCTAssertEqual(notebook.notes.count, 2)
        XCTAssertEqual([notebook.notes[0].uid,notebook.notes[1].uid], ["aaaa","dddd"])
    }
    
    func testSaveUploadNotes() {
        
        notebook.saveNotes()
        let uploadNotebook = DummyNotebook()
        uploadNotebook.uploadNotes()
        
        if notebook.notes.count == 3 {
            for index in 0...2 {
                XCTAssertEqual(notebook.notes[index].uid,uploadNotebook.notes[index].uid)
                XCTAssertEqual(notebook.notes[index].title,uploadNotebook.notes[index].title)
                XCTAssertEqual(notebook.notes[index].content,uploadNotebook.notes[index].content)
                XCTAssertEqual(notebook.notes[index].color,uploadNotebook.notes[index].color)
                XCTAssertEqual(notebook.notes[index].deleteDate,uploadNotebook.notes[index].deleteDate)
            }
        }
        else {
            XCTFail()
        }
        
    }
    
    func testAutoDelete() {
        notebook.addNewNote(note: Note(title: "title", content: "content", deleteDate: Date().addingTimeInterval(-30)))
        XCTAssertEqual(notebook.notes.count, 3)
        XCTAssertEqual([notebook.notes[0].uid,notebook.notes[1].uid,notebook.notes[2].uid], ["aaaa","ssss","dddd"])
        DispatchQueue.main.asyncAfter(deadline: .now()+30) {
            XCTAssertEqual(self.notebook.notes.count, 2)
            XCTAssertEqual([self.notebook.notes[0].uid,self.notebook.notes[1].uid], ["aaaa","ssss"])
        }
    }
    
    
}
