//
//  UIColor+HexTests.swift
//  Notes
//
//  Created by admin on 29.04.17.
//  Copyright © 2017 admin. All rights reserved.
//

import XCTest
@testable import Notes

class UIColor_HexTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testColorToHexString() {
        XCTAssertEqual(UIColor.red.toHexString,"FF0000")
        
    }
    
    func testHexStringToColor() {
        XCTAssertEqual(UIColor(hex: "FF0000"), UIColor.red)
        XCTAssertNil(UIColor(hex: "helloworld"))
        
    }
 
}
