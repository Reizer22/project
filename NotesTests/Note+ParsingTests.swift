//
//  Note+ParsingTests.swift
//  Notes
//
//  Created by admin on 27.04.17.
//  Copyright © 2017 admin. All rights reserved.
//

import XCTest
@testable import Notes

class Note_ParsingTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNoteParsing() {
        let testDate: Date = Date().addingTimeInterval(60*1)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzzz"
        formatter.timeZone = TimeZone.autoupdatingCurrent
        
        let fullNote = Note(uid: "klsdhfl-sfjk-awjnaas", title:"Заметка №1", content: "Текст заметки", color: UIColor.red, deleteDate: testDate);

        let fullJson: [String: Any] = ["uid": "klsdhfl-sfjk-awjnaas","title": "Заметка №1", "content": "Текст заметки", "color": "FF0000", "delete": formatter.string(from: testDate)];
        
        XCTAssertTrue(NSDictionary(dictionary: fullJson).isEqual(to: fullNote.json))
        
        let fullJsonNote = Note.parse(json: fullJson)
        
        XCTAssertEqual(fullJsonNote?.uid, fullNote.uid)
        XCTAssertEqual(fullJsonNote?.title, fullNote.title)
        XCTAssertEqual(fullJsonNote?.content, fullNote.content)
        XCTAssertEqual(fullJsonNote?.color, fullNote.color)
        XCTAssertEqual(formatter.string(from: (fullJsonNote?.deleteDate)!), formatter.string(from: fullNote.deleteDate!))
    }
    
}
